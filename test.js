'use strict';

const chess = require('./chess');

console.log('Verifying chess works for various encoded strings...');

const encodedStrings = [
  {
    test:'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR',
    result: [
      'rnbqkbnr',
      'pppppppp',
      '........',
      '........',
      '........',
      '........',
      'PPPPPPPP',
      'RNBQKBNR',
    ],
  },
  {
    test: 'r1bk3r/p2pBpNp/n4n2/1p1NP2P/6P1/3P4/P1P1K3/q5b1',
    result: [
      'r.bk...r',
      'p..pBpNp',
      'n....n..',
      '.p.NP..P',
      '......P.',
      '...P....',
      'P.P.K...',
      'q.....b.',
    ],
  },
  {
    test: 'k2r3p/r6p/k7/6n1/6k1/2p3p1/KKKKKKKK/nk6',
    result: [
      'k..r...p',
      'r......p',
      'k.......',
      '......n.',
      '......k.',
      '..p...p.',
      'KKKKKKKK',
      'nk......',
    ],
  },
];

encodedStrings.forEach((test, i) => {
  if (chess.decode(test.test).join('') === test.result.join('')) {
    return console.log(`Test ${i} passed`);
  }

  console.error('Test failed');
});





