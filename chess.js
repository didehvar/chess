'use strict';

const chess = module.exports = {};

const numOfRows = 8;
const rowLength = 8;
const validPieces = /^[pkqbnr]$/i;
const emptyPiece = '.';

function exit(message) {
  console.error(message);
  process.exit(1);
}

chess.decode = (encodedPosition) => {
  const rows = encodedPosition.split('/');
  if (rows.length !== numOfRows) return exit('Wrong number of rows');

  return rows.map((row, rowIndex) => {
    const pieces = row.split('').map(piece => {
      if (piece.match(validPieces)) return piece;

      const emptySpaces = Number.parseInt(piece);
      if (Number.isInteger(emptySpaces) && emptySpaces <= rowLength) {
        return emptyPiece.repeat(emptySpaces);
      }

      return exit(`Unexpected character: ${piece}`);
    }).join('');
  
    if (pieces.length !== rowLength) {
      return exit(`Wrong number of squares on row ${rowIndex + 1}`);
    }
    
    return pieces;
  });
}
