'use strict';

const readline = require('readline');
const chess = require('./chess');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question('Input encoded chess position string: ', encodedString => {
  console.log(chess.decode(encodedString).join('\n'));
  rl.close();
});
